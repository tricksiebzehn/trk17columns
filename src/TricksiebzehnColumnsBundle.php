<?php

namespace Tricksiebzehn\ColumnsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao Columns bundle.
 *
 * @author Flaggschiff
 */
class TricksiebzehnColumnsBundle extends Bundle
{
}
