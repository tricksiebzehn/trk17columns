<?php

if(TL_MODE=='BE') {
    if($GLOBALS['TL_CONFIG']['bootstrap']=='regular') {
        $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/be.min.css';
    }
    elseif($GLOBALS['TL_CONFIG']['bootstrap']=='regular_v4'){
        $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/be.4.min.css';
    }
    else{
        $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/be.trk17.min.css';
    }
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/tricksiebzehncolumns/js/be.js|static';
}

$GLOBALS['TL_CTE']['column']['colStart'] = 'Tricksiebzehn\ContentColumnStart';
$GLOBALS['TL_CTE']['column']['colStop'] = 'Tricksiebzehn\ContentColumnStop';

$GLOBALS['TL_HOOKS']['getContentElement'][] = array('Tricksiebzehn\Trk17Columns', 'getContentElementHook');
$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('Tricksiebzehn\Trk17Container','parseTemplateHook');
$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('Tricksiebzehn\Trk17Columns','parseTemplateHook');
$GLOBALS['TL_HOOKS']['parseWidget'][] = array('Tricksiebzehn\Trk17Forms', 'parseWidgetHook');
$GLOBALS['TL_HOOKS']['parseFrontendTemplate'][] = array('Tricksiebzehn\Trk17Container', 'parseFrontendTemplateHook');
