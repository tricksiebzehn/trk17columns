<?php

namespace Tricksiebzehn;
use Contao;

class Trk17Forms extends \Frontend {

    public function parseWidgetHook($strBuffer, $widget)
    {
        require_once(dirname(__FILE__) . '/Trk17ColumnsController.php');
        $this->Columns = new Trk17ColumnsController();

        if ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular') {
            $colElement = array(
                'trk17_responsive' => $widget->trk17_responsive,
                'sizes' => array(
                    'xs' => $widget->trk17_xs_columns,
                    'sm' => $widget->trk17_sm_columns,
                    'md' => $widget->trk17_md_columns,
                    'lg' => $widget->trk17_columns
                )
            );
            $addClass = $this->Columns->caclulateObjectColumns($colElement);

            if ($widget->type == 'fieldset'||$widget->type=='fieldsetStart'||$widget->type=='fieldsetStop') {
                $addClass .= ' row';
            }
        }
        if ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular_v4') {
            $colElement = array(
                'trk17_responsive' => $widget->trk17_responsive,
                'sizes' => array(
                    'xs4' => $widget->trk17_xs_columns,
                    'sm' => $widget->trk17_sm_columns,
                    'md' => $widget->trk17_md_columns,
                    'lg' => $widget->trk17_lg_columns,
                    'xl' => $widget->trk17_columns
                )
            );
            $addClass = $this->Columns->caclulateObjectColumns($colElement);
        }
        else {
            $colElement = array(
                'trk17_responsive' => $widget->trk17_responsive,
                'sizes' => array(
                    'xxs' => $widget->trk17_xxs_columns,
                    'xs' => $widget->trk17_xs_columns,
                    'sm' => $widget->trk17_sm_columns,
                    'md' => $widget->trk17_md_columns,
                    'lg' => $widget->trk17_lg_columns,
                    'xlg' => $widget->trk17_columns
                )
            );
            $addClass = $this->Columns->caclulateObjectColumns($colElement);

            if ($widget->type == 'fieldset'||$widget->type=='fieldsetStart'||$widget->type=='fieldsetStop') {
                $addClass .= ' row';
            }
        }
        if (TL_MODE == 'FE') {
            if($widget->type!='fieldset'&&$widget->type!='fieldsetStart'&&$widget->type!='fieldsetStop') {
                return '<div class="' . $addClass . '">' . $strBuffer . '</div>';
            }
            else{
                if($widget->fsType=='fsStart'||$widget->type=='fieldsetStart'){
                    return '<div class="' . $addClass . '">' .$strBuffer;
                }
                else{
                    return $strBuffer . '</div>';
                }
            }
        }
        else{
            $strBuffer .= '<div class="sizes" data-size="'.$addClass.'"></div>';
            return $strBuffer;
        }
    }

}

?>