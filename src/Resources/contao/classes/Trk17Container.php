<?php

namespace Tricksiebzehn;
use Contao;

class Trk17Container extends \Frontend {

    public function parseTemplateHook($objTemplate){

        if(TL_MODE=='FE') {
            if ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular') {
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/bootstrap.min.css|static';
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/container.min.css|static';
            }
            elseif ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular_v4') {
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/bootstrap.4.min.css|static';
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/container.4.min.css|static';
            }
            else{
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/bootstrap.trk17.min.css|static';
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/container.trk17.min.css|static';
            }
            if($GLOBALS['TL_CONFIG']['containertype'] == 'fluidcontainer') {
                $GLOBALS['TL_CSS'][] = 'bundles/tricksiebzehncolumns/css/fluid.min.css|static';
            }
        }

        if(substr($objTemplate->getName(),0,7) == 'fe_page'){
            /* container and row around left right and main? */
            $sections = $objTemplate->sections;
            $contentleft = $objTemplate->left;
            $contentright = $objTemplate->right;
            $contentheader = $objTemplate->header;
            $contentfooter = $objTemplate->footer;
            $contentmain = $objTemplate->main;
            $containerclass = 'container';
            if($GLOBALS['TL_CONFIG']['containertype'] == 'fluidcontainer') {
                $containerclass = 'container-fluid';
            }
            $rowb = '<div class="row">';
            $rowe = '</div>';
            $contentsections = array();
            global $objPage;
            $layout = \LayoutModel::findById($objPage->layout);
            $cleftwidth = $layout->trk17widthLeft;
            $crightwidth = $layout->trk17widthRight;
            $cmainwidth = 100;
            if($contentleft){
                $cmainwidth -= $cleftwidth;
            }
            else{
                $cleftwidth = 0;
            }
            if($contentright){
                $cmainwidth -= $crightwidth;
            }
            else{
                $crightwidth = 0;
            }
            foreach($sections as $key => $contentsection){
                $contentsections[$key] = '<div class="'.$containerclass.'">'.$rowb.$contentsection.$rowe.'</div>';
            }
            $overflowMain = "overflow: hidden;";
            if($cleftwidth==0&&$crightwidth==0){
                $overflowMain = "";
            }
            $contentleft = '<div style="width: '.$cleftwidth.'%;overflow: hidden;margin-left: -'.($cmainwidth+$cleftwidth).'%;float: left;"><div class="'.$containerclass.'">'.$rowb.$contentleft.$rowe.'</div></div>';
            $contentright = '<div style="width: '.$crightwidth.'%;overflow: hidden;float: left;"><div class="'.$containerclass.'">'.$rowb.$contentright.$rowe.'</div></div>';
            $contentheader = '<div class="'.$containerclass.'">'.$rowb.$contentheader.$rowe.'</div>';
            $contentfooter = '<div class="'.$containerclass.'">'.$rowb.$contentfooter.$rowe.'</div>';
            $contentmain = '<div style="width: '.$cmainwidth.'%;'.$overflowMain.'margin-left: '.$cleftwidth.'%;float: left;"><div class="'.$containerclass.'">'.$rowb.$contentmain.$rowe.'</div></div>';
            $objTemplate->sections = $contentsections;
            $objTemplate->left = $contentleft;
            $objTemplate->right = $contentright;
            $objTemplate->header = $contentheader;
            $objTemplate->footer = $contentfooter;
            $objTemplate->main = $contentmain;
        }
        elseif ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular_v4') {
            if(substr($objTemplate->getName(),0,11) == 'mod_article') {
                $objTemplate->class .= " col-12";
            }
            elseif(substr($objTemplate->getName(),0,12) == 'form_wrapper'){
                $objTemplate->class .= " col-12";
            }
        }

    }

    public function parseFrontendTemplateHook($content, $template){
        if($template=='mod_article'){
            $content = preg_replace('/"mod_article[^>]*( data-frontend-helper="[^"]*")/', '', $content);
        }
        elseif(substr($template,0,4)=='mod_'){
            $content = preg_replace('/"'.$template.'[^>]*( data-frontend-helper="[^"]*")/', '', $content);
        }
        return $content;
    }

}

?>