<?php

namespace Tricksiebzehn;
use Contao;

class Trk17Columns extends \Frontend {

    public function getContentElementHook($objRow, $strBuffer, $objElement){
        require_once(dirname(__FILE__).'/Trk17ColumnsController.php');
        $this->Columns = new Trk17ColumnsController();

        /* All notrk17columns palettes are ignored */
        if($objElement->Template->notrk17columns!=1&&$objElement->notrk17columns!=1) {
            /* Adds row to accordion-content, since Elements within the accordion-content will receive bootstrap-classes again */
            if ($objElement->type == "accordionStart") {
                $objElement->Template->accordion .= ' row';
            }
            else if($objRow->type == 'form'){
                $strBuffer = '<div class="row">'.$strBuffer.'</div>';
            }

            /* Added cols will differ depending on bootstrap version */
            if ($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular') {
                /* Regular Content-Elements have $objElement->Template and trk17_columns cannot be NULL when a Content-Element is saved */
                if ($objElement->Template && !is_null($objElement->Template->trk17_columns)) {
                    $colElement = array(
                        'trk17_responsive' => $objElement->Template->trk17_responsive,
                        'sizes' => array(
                            'xs' => $objElement->Template->trk17_xs_columns ? $objElement->Template->trk17_xs_columns : 12,
                            'sm' => $objElement->Template->trk17_sm_columns ? $objElement->Template->trk17_sm_columns : 12,
                            'md' => $objElement->Template->trk17_md_columns ? $objElement->Template->trk17_md_columns : 12,
                            'lg' => $objElement->Template->trk17_columns ? $objElement->Template->trk17_columns : 12
                        )
                    );
                    /* Function passes a String that can instantly be added as CSS Class */
                    $addClass = $this->Columns->caclulateObjectColumns($colElement);
                    $objElement->Template->class = $objElement->Template->class . $addClass;
                    if(class_exists("MadeYourDay\RockSolidFrontendHelper\FrontendHelperUser")){
                        // replacement function for FrontendHelper
                        $replacementPosition = strpos($strBuffer,'class="');
                        if($replacementPosition!==false) {
                            $toReplace = substr_replace($strBuffer, preg_replace("/ /", "", $addClass, 1) . " ", ($replacementPosition + 7), 0);
                            $strBuffer = $toReplace;
                        }
                    }
                    else{
                        $strBuffer = $objElement->Template->parse();
                    }
                }
                /* Modules and Backend Content-Elements don't always use Templates (i.e. wildcards and such) */
                elseif (($objElement->type == 'module'||$objRow->type == 'form'||TL_MODE=='BE')) {
                    $obj = $objElement->trk17_columns ? $objElement : $objRow;
                    if(!is_null($obj->trk17_columns)) {
                        $colElement = array(
                            'trk17_responsive' => $obj->trk17_responsive,
                            'sizes' => array(
                                'xs' => $obj->trk17_xs_columns ? $obj->trk17_xs_columns : 12,
                                'sm' => $obj->trk17_sm_columns ? $obj->trk17_sm_columns : 12,
                                'md' => $obj->trk17_md_columns ? $obj->trk17_md_columns : 12,
                                'lg' => $obj->trk17_columns ? $obj->trk17_columns : 12
                            )
                        );
                        /* Function passes a String that can instantly be added as CSS Class */
                        $addClass = $this->Columns->caclulateObjectColumns($colElement);
                        $strBuffer = '<div class="module_container ' . $addClass . '">' . $strBuffer . '</div>';
                    }
                }
            }
            elseif($GLOBALS['TL_CONFIG']['bootstrap'] == 'regular_v4') {
                /* Regular Content-Elements have $objElement->Template and trk17_columns cannot be NULL when a Content-Element is saved */
                if ($objElement->Template && !is_null($objElement->Template->trk17_columns)) {
                    $colElement = array(
                        'trk17_responsive' => $objElement->Template->trk17_responsive,
                        'sizes' => array(
                            'xs4' => $objElement->Template->trk17_xs_columns ? $objElement->Template->trk17_xs_columns : 12,
                            'sm' => $objElement->Template->trk17_sm_columns ? $objElement->Template->trk17_sm_columns : 12,
                            'md' => $objElement->Template->trk17_md_columns ? $objElement->Template->trk17_md_columns : 12,
                            'lg' => $objElement->Template->trk17_lg_columns ? $objElement->Template->trk17_lg_columns : 12,
                            'xl' => $objElement->Template->trk17_columns ? $objElement->Template->trk17_columns : 12
                        )
                    );
                    /* Function passes a String that can instantly be added as CSS Class */
                    $addClass = $this->Columns->caclulateObjectColumns($colElement);
                    $objElement->Template->class = $objElement->Template->class . $addClass;
                    if(class_exists("MadeYourDay\RockSolidFrontendHelper\FrontendHelperUser")){
                        // replacement function for FrontendHelper
                        $replacementPosition = strpos($strBuffer,'class="');
                        if($replacementPosition!==false) {
                            $toReplace = substr_replace($strBuffer, preg_replace("/ /", "", $addClass, 1) . " ", ($replacementPosition + 7), 0);
                            $strBuffer = $toReplace;
                        }
                    }
                    else{
                        $strBuffer = $objElement->Template->parse();
                    }
                }
                /* Modules and Backend Content-Elements don't always use Templates (i.e. wildcards and such) */
                elseif (($objElement->type == 'module'||$objRow->type == 'form'||TL_MODE=='BE')) {
                    $obj = $objElement->trk17_columns ? $objElement : $objRow;
                    if(!is_null($obj->trk17_columns)) {
                        $colElement = array(
                            'trk17_responsive' => $obj->trk17_responsive,
                            'sizes' => array(
                                'xs4' => $obj->trk17_xs_columns ? $obj->trk17_xs_columns : 12,
                                'sm' => $obj->trk17_sm_columns ? $obj->trk17_sm_columns : 12,
                                'md' => $obj->trk17_md_columns ? $obj->trk17_md_columns : 12,
                                'lg' => $obj->trk17_lg_columns ? $obj->trk17_lg_columns : 12,
                                'xl' => $obj->trk17_columns ? $obj->trk17_columns : 12
                            )
                        );
                        /* Function passes a String that can instantly be added as CSS Class */
                        $addClass = $this->Columns->caclulateObjectColumns($colElement);
                        $strBuffer = '<div class="module_container ' . $addClass . '">' . $strBuffer . '</div>';
                    }
                }
            }
            else{
                /* Regular Content-Elements have $objElement->Template and trk17_columns cannot be NULL when a Content-Element is saved */
                if ($objElement->Template && !is_null($objElement->Template->trk17_columns)) {
                    $colElement = array(
                        'trk17_responsive' => $objElement->Template->trk17_responsive,
                        'sizes' => array(
                            'xxs' => $objElement->Template->trk17_xxs_columns ? $objElement->Template->trk17_xxs_columns : 12,
                            'xs' => $objElement->Template->trk17_xs_columns ? $objElement->Template->trk17_xs_columns : 12,
                            'sm' => $objElement->Template->trk17_sm_columns ? $objElement->Template->trk17_sm_columns : 12,
                            'md' => $objElement->Template->trk17_md_columns ? $objElement->Template->trk17_md_columns : 12,
                            'lg' => $objElement->Template->trk17_lg_columns ? $objElement->Template->trk17_lg_columns : 12,
                            'xlg' => $objElement->Template->trk17_columns ? $objElement->Template->trk17_columns : 12
                        )
                    );
                    /* Function passes a String that can instantly be added as CSS Class */
                    $addClass = $this->Columns->caclulateObjectColumns($colElement);
                    $objElement->Template->class = $objElement->Template->class . $addClass;
                    if(class_exists("MadeYourDay\RockSolidFrontendHelper\FrontendHelperUser")){
                        // replacement function for FrontendHelper
                        $replacementPosition = strpos($strBuffer,'class="');
                        if($replacementPosition!==false) {
                            $toReplace = substr_replace($strBuffer, preg_replace("/ /", "", $addClass, 1) . " ", ($replacementPosition + 7), 0);
                            $strBuffer = $toReplace;
                        }
                    }
                    else{
                        $strBuffer = $objElement->Template->parse();
                    }
                }
                /* Modules and Backend Content-Elements don't always use Templates (i.e. wildcards and such) */
                elseif (($objElement->type == 'module'||$objRow->type == 'form'||TL_MODE=='BE')) {
                    $obj = $objElement->trk17_columns ? $objElement : $objRow;
                    if(!is_null($obj->trk17_columns)) {
                        $colElement = array(
                            'trk17_responsive' => $obj->trk17_responsive,
                            'sizes' => array(
                                'xxs' => $obj->trk17_xxs_columns ? $obj->trk17_xxs_columns : 12,
                                'xs' => $obj->trk17_xs_columns ? $obj->trk17_xs_columns : 12,
                                'sm' => $obj->trk17_sm_columns ? $obj->trk17_sm_columns : 12,
                                'md' => $obj->trk17_md_columns ? $obj->trk17_md_columns : 12,
                                'lg' => $obj->trk17_lg_columns ? $obj->trk17_lg_columns : 12,
                                'xlg' => $obj->trk17_columns ? $obj->trk17_columns : 12
                            )
                        );
                        /* Function passes a String that can instantly be added as CSS Class */
                        $addClass = $this->Columns->caclulateObjectColumns($colElement);
                        $strBuffer = '<div class="module_container ' . $addClass . '">' . $strBuffer . '</div>';
                    }
                }
            }

            /* Backend needs Javascript to use col-sizes, this is especially relevant for wrappers. Thus the cols are only added as data-attribute */
            if(TL_MODE=='BE'){
                $strBuffer .= '<div class="sizes" data-size="'.$addClass.'"></div>';
            }
        }

        return $strBuffer;

    }

    public function parseTemplateHook($objTemplate){
        require_once(dirname(__FILE__).'/Trk17ColumnsController.php');
        $this->Columns = new Trk17ColumnsController();

        if(TL_MODE=='FE'&&(($objTemplate->origCssID&&$objTemplate->ptable)||($objTemplate->origId))) {
            if($GLOBALS['TL_CONFIG']['bootstrap']=='regular'){
                $colElement = array(
                    'trk17_responsive' => $objTemplate->trk17_responsive,
                    'sizes' => array(
                        'xs' => $objTemplate->trk17_xs_columns ? $objTemplate->trk17_xs_columns : 12,
                        'sm' => $objTemplate->trk17_sm_columns ? $objTemplate->trk17_sm_columns : 12,
                        'md' => $objTemplate->trk17_md_columns ? $objTemplate->trk17_md_columns : 12,
                        'lg' => $objTemplate->trk17_columns ? $objTemplate->trk17_columns : 12
                    )
                );
                $addClass = $this->Columns->caclulateObjectColumns($colElement);
            }
            elseif($GLOBALS['TL_CONFIG']['bootstrap']=='regular_v4'){
                $colElement = array(
                    'trk17_responsive' => $objTemplate->trk17_responsive,
                    'sizes' => array(
                        'xs4' => $objTemplate->trk17_xs_columns ? $objTemplate->trk17_xs_columns : 12,
                        'sm' => $objTemplate->trk17_sm_columns ? $objTemplate->trk17_sm_columns : 12,
                        'md' => $objTemplate->trk17_md_columns ? $objTemplate->trk17_md_columns : 12,
                        'lg' => $objTemplate->trk17_lg_columns ? $objTemplate->trk17_lg_columns : 12,
                        'xl' => $objTemplate->trk17_columns ? $objTemplate->trk17_columns : 12
                    )
                );
                $addClass = $this->Columns->caclulateObjectColumns($colElement);
            }
            else{
                $colElement = array(
                    'trk17_responsive' => $objTemplate->trk17_responsive,
                    'sizes' => array(
                        'xxs' => $objTemplate->trk17_xxs_columns ? $objTemplate->trk17_xxs_columns : 12,
                        'xs' => $objTemplate->trk17_xs_columns ? $objTemplate->trk17_xs_columns : 12,
                        'sm' => $objTemplate->trk17_sm_columns ? $objTemplate->trk17_sm_columns : 12,
                        'md' => $objTemplate->trk17_md_columns ? $objTemplate->trk17_md_columns : 12,
                        'lg' => $objTemplate->trk17_lg_columns ? $objTemplate->trk17_lg_columns : 12,
                        'xlg' => $objTemplate->trk17_columns ? $objTemplate->trk17_columns : 12
                    )
                );
                $addClass = $this->Columns->caclulateObjectColumns($colElement);
            }
            $objTemplate->class .= ' '.$addClass;
        }
    }

}

?>