<?php

$GLOBALS['TL_LANG']['tl_layout']['trk17widthLeft'] = array('Breite der linken Spalte','Bitte geben Sie die Breite der linken Spalte ein. (in %)');
$GLOBALS['TL_LANG']['tl_layout']['trk17widthRight'] = array('Breite der rechten Spalte','Bitte geben Sie die Breite der rechten Spalte ein. (in %)');
$GLOBALS['TL_LANG']['SECTIONS']['main'] = '--';