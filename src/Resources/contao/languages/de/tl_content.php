<?php


$GLOBALS['TL_LANG']['tl_content']['trk17_columns'] = array('Breite des Elements','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_responsive'] = array('Responsive Größe','Möchten Sie die Breite für kleinere Gerätegrößen individuell anpassen?');
$GLOBALS['TL_LANG']['tl_content']['trk17_xxs_columns'] = array('Bei den kleinsten Geräten','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_xs_columns'] = array('Bei sehr kleinen Geräten ( ab 480px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_xs_columns2'] = array('Bei sehr kleinen Geräten','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_sm_columns'] = array('Bei kleinen Geräten ( ab 768px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_sm_columns_v4'] = array('Bei kleinen Geräten ( ab 576px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_md_columns'] = array('Bei mittelgroßen Geräten ( ab 992px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_md_columns2'] = array('Bei mittelgroßen Geräten ( ab 992px bis 1200px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_md_columns_v4'] = array('Bei mittelgroßen Geräten ( ab 768px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_lg_columns'] = array('Bei großen Geräten ( ab 1200px bis 1680px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['trk17_lg_columns_v4'] = array('Bei großen Geräten ( ab 992px bis 1200px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_content']['hidden'] = 'Versteckt';
$GLOBALS['TL_LANG']['tl_content']['full'] = 'Vollspaltig';
$GLOBALS['TL_LANG']['tl_content']['half'] = 'Halbspaltig';
$GLOBALS['TL_LANG']['tl_content']['third'] = 'Drittelspaltig';
$GLOBALS['TL_LANG']['tl_content']['quarter'] = 'Viertelspaltig';
$GLOBALS['TL_LANG']['tl_content']['two-third'] = '2/3-spaltig';
$GLOBALS['TL_LANG']['tl_content']['three-quarter'] = '3/4-spaltig';
$GLOBALS['TL_LANG']['tl_content']['colBackground'] = array('Hintergrund','Soll diese Spalte einen Hintergrund haben?');
$GLOBALS['TL_LANG']['tl_content']['colFullwidthBg'] = array('Volle Breite','Soll sich der Hintergrund über das gesamte Browserfenster ziehen?');
$GLOBALS['TL_LANG']['tl_content']['colBgColor'] = array('Hintergrundfarbe','Bitte geben Sie die Hintergrundfarbe der Spalte an.');
$GLOBALS['TL_LANG']['tl_content']['info_screensizes'] = 'Vorschau Bildschirmgrößen';

$GLOBALS['TL_LANG']['CTE']['colStart'] = array("Umschlag Anfang","");
$GLOBALS['TL_LANG']['CTE']['colStop'] = array("Umschlag Ende","");
$GLOBALS['TL_LANG']['CTE']['trk17divider'] = array("Trenner","");

$GLOBALS['TL_LANG']['tl_content']['trk17_columns_preview_reset'] = "Zurücksetzen";

?>