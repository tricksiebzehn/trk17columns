<?php


$GLOBALS['TL_LANG']['tl_form_field']['trk17_columns'] = array('Breite des Elements','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_responsive'] = array('Responsive Größe','Möchten Sie die Breite für kleinere Gerätegrößen individuell anpassen?');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_xxs_columns'] = array('Bei den kleinsten Geräten','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_xs_columns'] = array('Bei sehr kleinen Geräten ( ab 480px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_xs_columns2'] = array('Bei sehr kleinen Geräten','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_sm_columns'] = array('Bei kleinen Geräten ( ab 768px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_sm_columns_v4'] = array('Bei kleinen Geräten ( ab 576px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns'] = array('Bei mittelgroßen Geräten ( ab 992px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns2'] = array('Bei mittelgroßen Geräten ( ab 992px bis 1200px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns_v4'] = array('Bei mittelgroßen Geräten ( ab 768px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_lg_columns'] = array('Bei großen Geräten ( ab 1200px bis 1680px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['trk17_lg_columns_v4'] = array('Bei großen Geräten ( ab 992px bis 1200px Breite )','Bitte geben Sie die Breite des Elements an.');
$GLOBALS['TL_LANG']['tl_form_field']['full'] = 'Vollspaltig';
$GLOBALS['TL_LANG']['tl_form_field']['half'] = 'Halbspaltig';
$GLOBALS['TL_LANG']['tl_form_field']['third'] = 'Drittespaltig';
$GLOBALS['TL_LANG']['tl_form_field']['quarter'] = 'Viertelspaltig';
$GLOBALS['TL_LANG']['tl_form_field']['two-third'] = '2/3-spaltig';
$GLOBALS['TL_LANG']['tl_form_field']['three-quarter'] = '3/4-spaltig';

?>