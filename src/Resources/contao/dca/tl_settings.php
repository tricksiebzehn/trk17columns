<?php

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{trk17columns_legend},containertype,bootstrap,trk17col_sizes';

$GLOBALS['TL_DCA']['tl_settings']['fields']['containertype'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['containertype'],
    'exclude'                 => true,
    'default'                 => 'regularcontainer',
    'inputType'               => 'select',
    'options'                 => array('regularcontainer','fluidcontainer'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_settings']
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['bootstrap'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['bootstrap'],
    'exclude'                 => true,
    'default'                 => 'tricksiebzehn',
    'inputType'               => 'select',
    'options'                 => array('tricksiebzehn','regular','regular_v4'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_settings']
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['trk17col_sizes'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['trk17col_sizes'],
    'exclude'                 => true,
    'default'                 => 'preset',
    'inputType'               => 'select',
    'options'                 => array('preset','all'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_settings']
);