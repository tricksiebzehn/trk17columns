<?php

$GLOBALS['TL_DCA']['tl_form_field']['config']['onload_callback'][] = array('tl_form_field_trk17_columns','addTrk17ColumnsDCA');
$GLOBALS['TL_DCA']['tl_form_field']['config']['onsubmit_callback'][] = array('tl_form_field_trk17_columns','removeColValues');

$trk17coloptions = array('full', 'half', 'third', 'quarter', 'two-third', 'three-quarter', 'hidden');

if($GLOBALS['TL_CONFIG']['trk17col_sizes']=='all'){
    $trk17coloptions = array(12,11,10,9,8,7,6,5,4,3,2,1,'hidden');
}

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'eval' => array('submitOnChange' => true, 'tl_class'=>'w50 clr'),
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']=='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_columns']['eval']['submitOnChange'] = false;
}
$GLOBALS['TL_DCA']['tl_form_field']['palettes']['__selector__'][] = 'trk17_responsive';

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_responsive'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_responsive'],
    'exclude' => false,
    'inputType' => 'checkbox',
    'eval' => array('submitOnChange' => true, 'tl_class'=>'w50 clr'),
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xxs_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_xxs_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'eval' => array('tl_class'=>'w50 clr'),
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xxs_columns']['load_callback'] = array(array('tl_form_field_trk17_columns', 'loadXXSColumns'));
}

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xs_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_xs_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'eval' => array('tl_class'=>'w50 clr'),
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xs_columns']['load_callback'] = array(array('tl_form_field_trk17_columns', 'loadXSColumns'));
}

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_sm_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_sm_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'eval' => array('tl_class'=>'w50 clr'),
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_sm_columns']['load_callback'] = array(array('tl_form_field_trk17_columns', 'loadSMColumns'));
}

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_md_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'eval' => array('tl_class'=>'w50 clr'),
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_md_columns']['load_callback'] = array(array('tl_form_field_trk17_columns', 'loadMDColumns'));
}

$GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_lg_columns'] = array(
    'exclude' => false,
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['trk17_lg_columns'],
    'inputType' => 'select',
    'options' => $trk17coloptions,
    'reference' => &$GLOBALS['TL_LANG']['tl_form_field'],
    'eval' => array('tl_class'=>'w50 clr'),
    'sql' => "varchar(32) NOT NULL default ''"
);
if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_lg_columns']['load_callback'] = array(array('tl_form_field_trk17_columns', 'loadLGColumns'));
}

if($GLOBALS['TL_CONFIG']['bootstrap']=='regular'){
    $GLOBALS['TL_DCA']['tl_form_field']['subpalettes']['trk17_responsive'] = 'trk17_md_columns,trk17_sm_columns,trk17_xs_columns';
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xs_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_xs_columns2'];
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_md_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns2'];
}
if($GLOBALS['TL_CONFIG']['bootstrap']=='regular_v4'){
    $GLOBALS['TL_DCA']['tl_form_field']['subpalettes']['trk17_responsive'] = 'trk17_lg_columns,trk17_md_columns,trk17_sm_columns,trk17_xs_columns';
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_xs_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_xs_columns2'];
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_sm_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_sm_columns_v4'];
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_md_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_md_columns_v4'];
    $GLOBALS['TL_DCA']['tl_form_field']['fields']['trk17_lg_columns']['label'] = &$GLOBALS['TL_LANG']['tl_form_field']['trk17_lg_columns_v4'];
}
else{
    $GLOBALS['TL_DCA']['tl_form_field']['subpalettes']['trk17_responsive'] = 'trk17_lg_columns,trk17_md_columns,trk17_sm_columns,trk17_xs_columns,trk17_xxs_columns';
}

class tl_form_field_trk17_columns extends \Backend
{

    public function __construct()
    {
        parent::__construct();
    }

    public function loadLGColumns($val,$dc){
        $return = $val;
        if(!$val && $GLOBALS['TL_CONFIG']['trk17col_sizes']!='all'){
            $trk17_columns = $dc->trk17_columns;
            if(!$trk17_columns){
                $trk17_columns = $dc->activeRecord->trk17_columns;
            }
            switch($trk17_columns){
                case 'full':
                    $return = 'full';
                    break;
                case 'half':
                    $return = 'half';
                    break;
                case 'third':
                    $return = 'third';
                    break;
                case 'quarter':
                    $return = 'quarter';
                    break;
                case 'two-third':
                    $return = 'two-third';
                    break;
                case 'three-quarter':
                    $return = 'three-quarter';
                    break;
                case 'hidden':
                    $return = 'hidden';
                    break;
            }
            if($return!=$val) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_lg_columns=? WHERE id=?")->execute($return, $dc->id);
            }
        }
        return $return;
    }

    public function loadMDColumns($val,$dc){
        $return = $val;
        if(!$val && $GLOBALS['TL_CONFIG']['trk17col_sizes']!='all'){
            $trk17_columns = $dc->trk17_columns;
            if(!$trk17_columns){
                $trk17_columns = $dc->activeRecord->trk17_columns;
            }
            switch($trk17_columns){
                case 'full':
                    $return = 'full';
                    break;
                case 'half':
                    $return = 'half';
                    break;
                case 'third':
                    $return = 'third';
                    break;
                case 'quarter':
                    $return = 'quarter';
                    break;
                case 'two-third':
                    $return = 'two-third';
                    break;
                case 'three-quarter':
                    $return = 'three-quarter';
                    break;
                case 'hidden':
                    $return = 'hidden';
                    break;
            }
            if($return!=$val) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_md_columns=? WHERE id=?")->execute($return, $dc->id);
            }
        }
        return $return;
    }

    public function loadSMColumns($val,$dc){
        $return = $val;
        if(!$val && $GLOBALS['TL_CONFIG']['trk17col_sizes']!='all'){
            $trk17_columns = $dc->trk17_columns;
            if(!$trk17_columns){
                $trk17_columns = $dc->activeRecord->trk17_columns;
            }
            switch($trk17_columns){
                case 'full':
                    $return = 'full';
                    break;
                case 'half':
                    $return = 'half';
                    break;
                case 'third':
                    $return = 'half';
                    break;
                case 'quarter':
                    $return = 'half';
                    break;
                case 'two-third':
                    $return = 'half';
                    break;
                case 'three-quarter':
                    $return = 'half';
                    break;
                case 'hidden':
                    $return = 'hidden';
                    break;
            }
            if($return!=$val) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_sm_columns=? WHERE id=?")->execute($return, $dc->id);
            }
        }
        return $return;
    }

    public function loadXSColumns($val,$dc){
        $return = $val;
        if(!$val && $GLOBALS['TL_CONFIG']['trk17col_sizes']!='all'){
            $trk17_columns = $dc->trk17_columns;
            if(!$trk17_columns){
                $trk17_columns = $dc->activeRecord->trk17_columns;
            }
            switch($trk17_columns){
                case 'full':
                    $return = 'full';
                    break;
                case 'half':
                    $return = 'full';
                    break;
                case 'third':
                    $return = 'full';
                    break;
                case 'quarter':
                    $return = 'full';
                    break;
                case 'two-third':
                    $return = 'full';
                    break;
                case 'three-quarter':
                    $return = 'full';
                    break;
                case 'hidden':
                    $return = 'hidden';
                    break;
            }
            if($return!=$val) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_xs_columns=? WHERE id=?")->execute($return, $dc->id);
            }
        }
        return $return;
    }

    public function loadXXSColumns($val,$dc){
        $return = $val;
        if(!$val && $GLOBALS['TL_CONFIG']['trk17col_sizes']!='all'){
            $trk17_columns = $dc->trk17_columns;
            if(!$trk17_columns){
                $trk17_columns = $dc->activeRecord->trk17_columns;
            }
            switch($trk17_columns){
                case 'full':
                    $return = 'full';
                    break;
                case 'half':
                    $return = 'full';
                    break;
                case 'third':
                    $return = 'full';
                    break;
                case 'quarter':
                    $return = 'full';
                    break;
                case 'two-third':
                    $return = 'full';
                    break;
                case 'three-quarter':
                    $return = 'full';
                    break;
                case 'hidden':
                    $return = 'hidden';
                    break;
            }
            if($return!=$val) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_xxs_columns=? WHERE id=?")->execute($return, $dc->id);
            }
        }
        return $return;
    }

    public function removeColValues($dc){
        if($GLOBALS['TL_CONFIG']['trk17col_sizes']!='all') {
            $trk17_responsive = $dc->activeRecord->trk17_responsive;
            if (!$trk17_responsive) {
                $this->Database->prepare("UPDATE tl_form_field SET trk17_xxs_columns='', trk17_xs_columns='', trk17_sm_columns='', trk17_md_columns='', trk17_lg_columns='' WHERE id=?")->execute($dc->id);
            }
        }
    }

    public function addTrk17ColumnsDCA()
    {
        foreach ($GLOBALS['TL_DCA']['tl_form_field']['palettes'] as &$GPalette) {
            if (!is_array($GPalette)) {

                if (strpos($GPalette, ',notrk17columns')) {
                    $GPalette = str_replace(',notrk17columns', '', $GPalette);
                } else {
                    $GPalette = str_replace(',type,', ',type,trk17_columns,trk17_responsive,', $GPalette);
                    $GPalette = str_replace(',type;', ',type,trk17_columns,trk17_responsive;', $GPalette);
                    if($GLOBALS['TL_CONFIG']['trk17col_sizes']=='all'){
                        $GPalette = str_replace('trk17_columns,trk17_responsive,trk17_columns,trk17_responsive', 'trk17_columns,trk17_responsive', $GPalette);
                    }
                }
            }
        }

    }

}

?>