<?php

$GLOBALS['TL_DCA']['tl_layout']['subpalettes']['cols_2cll'] = 'trk17widthLeft';
$GLOBALS['TL_DCA']['tl_layout']['subpalettes']['cols_2clr'] = 'trk17widthRight';
$GLOBALS['TL_DCA']['tl_layout']['subpalettes']['cols_3cl'] = 'trk17widthLeft,trk17widthRight';

$GLOBALS['TL_DCA']['tl_layout']['fields']['trk17widthLeft'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_layout']['trk17widthLeft'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_layout']['fields']['trk17widthRight'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_layout']['trk17widthRight'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);
